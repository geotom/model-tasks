## Description

(Describe the merge request)

## Conformity
- [ ] Provided description
- [ ] Added test
- [ ] Added demo
- [ ] [Updated documentation](https://gitlab.com/geotom/model-tasks/-/wikis/home)
- [ ] Ran tests locally
