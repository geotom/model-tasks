import logging
from modeltasks import Model, ModelTask
from modeltasks.data import VariableInput, VariableOutput
from modeltasks.config import ConfigurationValue


if __name__ == "__main__":
    model = Model(
        title='Model 1',
        model_tasks=__file__,
        model_config=dict(
            VARIABLE_1=1.0,
            VARIABLE_2=2.0,
            VARIABLE_3=3.0,
            VARIABLE_4=4.0
        ),
        scheduler=dict(
            run_concurrent=True,
            max_threads=4
        ),
        log_level=logging.INFO,
    )


class TaskOne(ModelTask):

    # Configs
    v1: ConfigurationValue = 'VARIABLE_1'
    v2: ConfigurationValue = 'VARIABLE_2'
    v3: ConfigurationValue = 'VARIABLE_3'

    # Outputs
    v_sum: VariableOutput

    def run(self, logger, workspace):
        self.v_sum = self.v1 + self.v2 + self.v3
        logger.info(f'Sum of all test variables = {self.v_sum}')


class TaskTwo(ModelTask):

    # Configs
    v4: ConfigurationValue = 'VARIABLE_4'

    # Inputs
    sum: VariableInput = TaskOne

    # Outputs
    v_product: VariableOutput

    def run(self, logger, workspace):
        self.v_product = self.v4 * self.sum
        logger.info(f'Product of inputs = {self.v_product}')


class TaskThree(ModelTask):

    # Inputs
    sum: VariableInput = TaskOne

    # Outputs
    v_division: VariableOutput

    def run(self, logger, workspace):
        self.v_division = self.sum / self.sum
        logger.info(f'Division of inputs = {self.v_division}')


class TaskFour(ModelTask):

    # Inputs
    v_product: VariableInput = TaskTwo
    v_division: VariableInput = TaskThree

    # Outputs
    v_combined: VariableOutput

    def run(self, logger, workspace):
        self.v_combined = self.v_product + self.v_division
        logger.info(f'Combined inputs = {self.v_combined}')
